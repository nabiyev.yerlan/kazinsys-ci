# Скрипты для автоматического резервного копирования сайтов

- `backupdb` - скрипт для резервного копирования баз данных
- `backupsite` - скрипт для резервного копирования сайтов

## Необходимое ПО

- jq - утилита для чтения json

```shell
apt install jq
```

- minio client - клиент для загрузки файлов в удаленное хранилище Minio

```shell
curl https://dl.min.io/client/mc/release/linux-amd64/mc \
--create-dirs \
-o $HOME/minio-binaries/mc

chmod +x $HOME/minio-binaries/mc
```

## Настройка удаленного хранилища

Запросите логин и пароль от хранилища для сервера
```shell
~/minio-binaries/mc alias set kazinsys https://storage.kazinsys.kz <LOGIN> <PASSWORD>
```

## Конфигурирование
Скрипты и конфигурацию необходимо поместить в домашнюю директорию пользователя от 
которого будет делаться резервная копия

```shell
cd ~/

wget https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/backup/backupdb
wget https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/backup/backupsite
wget https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/backup/.backup.config.json

chmod +x backupdb
chmod +x backupsite
```



#### Описание конфигурации
В случае если на сервере имеется несколько сайтов, то можно добавить дополнительные конфигурации
с другими ключами (не default)
```json
{
    "storage_path": "/backups",             // Локальный путь по которому будут храниться бекапы
    "mysql": {                              // Конфигурации БД
        "default": {                        // Стандартная конфигурация
            "user": "gov4c",                // Пользователь БД
            "password": "password",         // Пароль от БД
            "database": "gov4c",            // Название БД
            "upload": true,                 // Загружать ли резервную копию в удаленное хранилище
            "save_last": 7                  // Сколько последних резервных копий сохранять на локальном диске
        }
    },
    "sites": {                              // Конфигурации бекапов файлов
        "default": {                        // Стандартная конфигурация
            "name": "gov4c_kz",             // Название бекапа
            "path": "/var/www/gov4c.kz",    // Директория, которая будет заархивирована
            "excluded": [                   // Массив путей, исключенных из резервного копирования
                "bitrix/backup/*",          
                ".git/*"                    
            ],                              
            "upload": true,                 // Загружать ли резервную копию в удаленное хранилище
            "save_last": 3                  // Сколько последних резервных копий сохранять на локальном диске
        }
    },
    "s3": {                                 // Конфигурация удаленного S3-хранилища
        "alias": "kazinsys",                // Название алиаса соединения (оставить также)
        "bucket": "backups",                // Название бакета (оставить также)
        "path": "gov4c/"                    // Путь в бакете (изменить на название сайта)
    }
}
```

## Запуск резервного копирования

```shell
# Бекап БД
~/backupdb default

# Бекап сайта
~/backupsite default
```

## Добавление в Cron


```shell
nano /etc/crontab
```
```
# Ежедневное создание бекапа БД в 3:00 ночи
0 3 * * *  <ПОЛЬЗОВАТЕЛЬ>  /<HOME>/backupdb default

# Еженедельное создание бекапа файлов в 3:00 в субботу
0 3 * * 6  <ПОЛЬЗОВАТЕЛЬ>  /<HOME>/backupsite default
```
