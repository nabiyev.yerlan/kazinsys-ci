#!/bin/bash

if [ -n "$(git status --porcelain)" ]; then
  echo "Cannot deploy - changes on production"
  git status --porcelain
  exit 1
else
  echo "no changes"
  git pull origin master
  exit 0
fi
