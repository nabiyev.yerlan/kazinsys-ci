#!/bin/bash

PROJECT_ID=$1
ACCESS_TOKEN=$2

BODY="{
    \"id\": ${PROJECT_ID},
    \"source_branch\": \"production\",
    \"target_branch\": \"master\",
    \"remove_source_branch\": true,
    \"title\": \"Production Changes\"
}"

# Проверяем наличие изменений
if [ -n "$(git status --porcelain)" ]; then
  echo "There are changes"
  git status --porcelain

  # Пушим изменения в ветку production
  git branch -d production
  git add .
  git commit -m 'Production changes'
  git checkout -b production
  git push origin production
  git checkout master
  git branch -d production
  echo "Pushed changes to 'production' branch"

  # Создание Pull Request из production в master
  curl -X POST \
    --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" \
    --header "Content-Type: application/json" \
    --data "${BODY}" \
    https://gitlab.com/api/v4/projects/"${PROJECT_ID}"/merge_requests
  exit 1
else
  echo "No changes found. Skipping."
  exit 0
fi
