#!/bin/bash

curl -LO https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/bitrix-projects/push-prod.sh
curl -LO https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/bitrix-projects/create-merge-request.sh

chmod +x push-prod.sh
chmod +x create-merge-request.sh

CMD="cd ${PRODUCTION_ROOTPATH} && bash -s"

ssh -p ${PRODUCTION_SSH_PORT} ${PRODUCTION_USER}@${PRODUCTION_HOST} "${CMD}" < ./push-prod.sh

status=$?

[ $status -eq 2 ] && ./create-merge-request.sh $CI_PROJECT_ID $GITLAB_API_TOKEN

exit 0
