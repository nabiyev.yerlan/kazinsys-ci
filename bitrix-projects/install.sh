#!/bin/bash

CUR_DIR=$1

# Копируем .env
test -f .env.local || cp .env.example .env.local

docker run --rm \
  --volume ${PWD}:/app \
  --volume ${HOME}/.config/composer:/tmp \
  --volume /etc/passwd:/etc/passwd:ro \
  --volume /etc/group:/etc/group:ro \
  --user $(id -u):$(id -g) \
  --interactive \
  composer composer install

docker run --rm \
  --volume /etc/passwd:/etc/passwd:ro \
  --volume /etc/group:/etc/group:ro \
  --user "$(id -u)":"$(id -g)" \
  --volume "${CUR_DIR}":/nodejs \
  --workdir /nodejs \
  --env HOME=/nodejs \
  --interactive \
  node:14 npm install

docker run --rm \
  --volume /etc/passwd:/etc/passwd:ro \
  --volume /etc/group:/etc/group:ro \
  --user "$(id -u)":"$(id -g)" \
  --volume "${CUR_DIR}":/nodejs \
  --workdir /nodejs \
  --env HOME=/nodejs \
  --interactive \
  node:14 npm run build

bash <(curl -s https://gitlab.com/kravets1996/kazinsys-ci/-/raw/main/dbsync)

export UID && docker-compose up -d
