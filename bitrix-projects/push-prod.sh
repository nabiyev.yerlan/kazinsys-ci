#!/bin/bash

# Если изменения есть где-то кроме папки sites/
if [ -n "$(git status --porcelain . -- ':!sites')" ]; then
  # Ничего не делаем и выходим с ошибкой
  # Нужно заходить на сервер и проверять вручную, что там изменили
  echo "Changes outside of sites/ folder"
  echo "SSH to your server and commit changes manually"
  exit 2
fi

# Проверяем наличие изменений в папке sites/ (ранее мы проверили изменения в любых других папках)
if [ -n "$(git status --porcelain)" ]; then
  # Пушим изменения в ветку production
  echo "There are changes"
  git branch -d production
  git add sites/
  git commit -m 'Production changes'
  git checkout -b production
  git push origin production
  git checkout master
  git branch -d production
  echo "Pushed changes to 'production' branch"
  git status --porcelain
  exit 1
else
  echo "No changes found. Skipping."
  exit 0
fi
